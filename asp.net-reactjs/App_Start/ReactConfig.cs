using JavaScriptEngineSwitcher.Core;
using JavaScriptEngineSwitcher.V8;
using React;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Asp.Net.ReactJS.ReactConfig), "Configure")]

namespace Asp.Net.ReactJS
{
	public static class ReactConfig
	{
		public static void Configure()
		{
            ReactSiteConfiguration.Configuration
                .SetAllowJavaScriptPrecompilation(true)
                .SetBabelVersion(BabelVersions.Babel7)
                    .AddScript("~/assets/js/components/SelectionPriority.js");

            JsEngineSwitcher.Current.DefaultEngineName = V8JsEngine.EngineName;
            JsEngineSwitcher.Current.EngineFactories.AddV8();
        }
	}
}