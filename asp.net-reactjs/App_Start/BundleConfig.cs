﻿using System.Web;
using System.Web.Optimization;
using System.Web.Optimization.React;

namespace Asp.Net.ReactJS
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/react").Include(
                       "~/Scripts/react/react.js"));

            bundles.Add(new ScriptBundle("~/bundles/react-dom").Include(
                       "~/Scripts/react/react-dom.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new BabelBundle("~/bundles/main").Include(
                // Add your JSX files here
                "~/assets/js/components/SelectionPriority.js"
            ));

            // Force minification/combination even in debug mode
            //BundleTable.EnableOptimizations = true;
        }
    }
}
