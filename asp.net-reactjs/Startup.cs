﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Asp.Net.ReactJS.Startup))]
namespace Asp.Net.ReactJS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {

            ConfigureAuth(app);
            //ReactConfig.Configure();
        }
    }
}
