﻿import React from '../../../Scripts/react/react.js';

class FormTask extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            List: []
        }
    }

    componentDidMount() {
        $.ajax({
            url: '/task/GetAllPriorities',
            success: function (r) {
                this.setState({ List: r });
            }
        })
    }

    render() {
        return (
            <div>
                {this.state.List}
            </div>
            );
    }
}