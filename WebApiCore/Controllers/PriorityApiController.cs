﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DatabaseCore.Interfaces;
using DatabaseCore.Structure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApiCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PriorityApiController : ControllerBase
    {
        private IPriorityRepository _priorityRepository;
        public PriorityApiController(IPriorityRepository priorityRepository)
        {
            _priorityRepository = priorityRepository;
        }

        // GET api/values
        [HttpGet]
        public ActionResult Get()
        {
            var allPriorities = _priorityRepository.GetAll().Result;
            return Ok(allPriorities);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            var priority = _priorityRepository.Get(id).Result;
            if (priority != null)
                return Ok(priority);

            return BadRequest("Null");
        }

        // POST api/values
        [HttpPost]
        public ActionResult<string> Post([FromBody] Priority value)
        {
            var newRecord = _priorityRepository.Create(value);
            if (newRecord == null)
            {
                return BadRequest();
            }
            else
            {
                return Ok(newRecord);
            }
        }

        // PUT: api/PriorityApi/5
        [HttpPut("{id}")]
        public ActionResult<Priority> Put(int id, [FromBody] Priority value)
        {
            value.Id = id;
            var result = _priorityRepository.Update(value);
            if (result.Result == null) return BadRequest();
            return Ok(result.Result);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public ActionResult<bool> Delete(int id)
        {
            var result = _priorityRepository.Delete(id);
            return result.Result;
        }
    }
}
