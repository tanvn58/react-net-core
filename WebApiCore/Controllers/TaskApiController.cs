﻿using DatabaseCore.Interfaces;
using DatabaseCore.Structure;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace WebApiCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskApiController : ControllerBase
    {
        private ITaskRepository _taskRepository;
        private IPriorityRepository _priorityRepository;
        public TaskApiController(ITaskRepository taskRepository, IPriorityRepository priorityRepository)
        {
            _taskRepository = taskRepository;
            _priorityRepository = priorityRepository;
        }
        // GET api/values
        [HttpGet]
        public ActionResult Get()
        {
            var allPriorities = _taskRepository.GetModels();
            return Ok(allPriorities);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            var task = _taskRepository.GetModel(id);
            if (task != null)
                return Ok(task);

            return BadRequest("Null");
        }

        // POST api/values
        [HttpPost]
        public ActionResult<string> Post([FromBody] Task value)
        {
            var newRecord = _taskRepository.Create(value).Result;
            if (newRecord == null)
            {
                return BadRequest();
            }
            else
            {
                var model = _taskRepository.GetModel(newRecord.Id);
                return Ok(model);
            }
        }

        // PUT: api/TaskApi/5
        [HttpPut("{id}")]
        public ActionResult<Task> Put(int id, [FromBody] Task value)
        {
            value.Id = id;
            var result = _taskRepository.Update(value);
            if (result.Result == null) return BadRequest();
            return Ok(result.Result);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public ActionResult<bool> Delete(int id)
        {
            var result = _taskRepository.Delete(id);
            return result.Result;
        }
    }
}
