import React from 'react';
import Priority from './Priority';
import { Form } from 'react-bootstrap'
import { Button } from 'react-bootstrap'
import axios from 'axios'

class CreateTaskForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      task: {
        name: '',
        description: '',
        priorityId: 0,
        createdBy: 0,
      },
      priorities: [],
      currentPriority: {}
    }
  }

  async componentDidMount() {
    var priorities = (await axios.get('http://localhost:5000/api/priorityapi')).data;
    priorities.sort((a, b) => { return a.rank - b.rank; })
    const task = this.state.task;
    task.priorityId = priorities[0].id;
    this.setState({task: task, priorities: priorities, currentPriority: priorities[0]});
  }

  onUpdateName = (e) => {
    const val = e.target.value;
    const task = this.state.task;
    task.name = val;
    this.setState({task: task});
  }

  onUpdateDescription = (e) => {
    const val = e.target.value;
    const task = this.state.task;
    task.description = val;
    this.setState({task: task});
  }

  onUpdatePriority = (priority) => {
    const task = this.state.task;
    task.priorityId = priority.id;
    this.setState({task: task, currentPriority: priority});
  }

  onCreate = () => {
    this.props.onCreate(this.state.task);
  }

  render() {
    return (
      <div className="row">
        <div className="col-12">

          <div className="row">
            <div className="col-6 col-xs-12">
              <Form.Control type="text" placeholder="Enter New Task Name ..." onInput={this.onUpdateName}/>
            </div>

            <div className="col-6 col-xs-12">
              <Priority priorities={this.state.priorities} currentPriority={this.state.currentPriority} onInput={this.onUpdatePriority}/>
            </div>
          </div>
          <br></br>
          <div className="row">
            <div className="col-12">
              <Form.Control as="textarea" rows="3" placeholder="Enter the description ..." onInput={this.onUpdateDescription}/>
            </div>
          </div>
          <br></br>
          <div className="row">
            <div className="col-12">
              <Button className="max-width btn-success" onClick={this.onCreate}>Add New</Button>
            </div>
          </div>

        </div>
      </div>
    );
  }
}

export default CreateTaskForm;