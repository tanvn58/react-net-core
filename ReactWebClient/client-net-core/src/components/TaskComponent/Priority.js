import React from 'react'
import { Dropdown } from 'react-bootstrap'
import './Task.scss'

class Priority extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPriority: "",
    }
  }

  componentDidMount() {
    this.setState({currentPriority: this.props.currentPriority})
  }

  componentWillReceiveProps(nextProps){
    this.setState({currentPriority: nextProps.currentPriority})
  }

  onSelect(eventKey, e) {
    var current = this.props.priorities.filter(x => x.rank == eventKey);
    this.setState({currentPriority: current[0]});
    this.props.onInput(current[0]);
  }

  render() {
    return (
      <Dropdown className="max-width" onSelect={this.onSelect.bind(this)} id={this.props.id}>
        <Dropdown.Toggle  className="max-width" variant="secondary">
          {this.state.currentPriority != undefined ? this.state.currentPriority.name : ""}
        </Dropdown.Toggle>

        <Dropdown.Menu  className="max-width">
          {this.props.priorities.map(x => (
            <Dropdown.Item href="#/" eventKey={x.rank} key={"priority-newform-" + x.id}>{x.name}</Dropdown.Item>
          ))}
        </Dropdown.Menu>
      </Dropdown>
    );
  }
}

export default Priority;