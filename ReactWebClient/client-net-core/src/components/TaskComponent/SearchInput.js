import React from 'react'
import { Form } from 'react-bootstrap'

const { Control } = Form

export const SearchInput = ({ onInput }) => {
  return (
    <Control
      type="text"
      onInput={onInput}
      placeholder="Enter Task Name ..."
    />
  )
}