import React from 'react'
import { Dropdown } from 'react-bootstrap'
import axios from 'axios'


export class PriorityFilter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPriority: 'All',
      priorities: []
    }
  }

  async componentDidMount() {
    var priorities = (await axios.get('http://localhost:5000/api/priorityapi')).data;
    priorities.sort((a, b) => { return a.rank - b.rank; })
    this.setState({ priorities: priorities })
  }

  onSelect(eventKey, e) {
    var current = this.state.priorities.filter(x => x.rank == eventKey);
    if (eventKey == "") this.setState({currentPriority: "All"})
    else this.setState({currentPriority: current[0].name})
    this.props.onSelect(eventKey, e);
  }

  render() {
    return (
      <Dropdown className={this.props.className} onSelect={this.onSelect.bind(this)}>
        <Dropdown.Toggle variant="secondary" className={this.props.className}>
          {this.state.currentPriority}
        </Dropdown.Toggle>

        <Dropdown.Menu className={this.props.className}>
          <Dropdown.Item href="#/all" eventKey="" key={"priority-all"}>All</Dropdown.Item>
          {this.state.priorities.map(x => (
            <Dropdown.Item href="#/action-1" eventKey={x.rank} key={"priority-" + x.id}>{x.name}</Dropdown.Item>
          ))}
        </Dropdown.Menu>
      </Dropdown>
    );
  }
}