import React from 'react';
import axios from 'axios';
import superagent from 'superagent'
import './Task.scss'
import { SearchInput } from './SearchInput'
import { TaskList } from './TaskList'
import { PriorityFilter } from './PriorityFilter'
import CreateTaskForm from './CreateTaskForm'

class Task extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tasks: [],
      filter: '',
      filterBy: ''
    }
  }

  async componentDidMount() {
    try {
      const res = await axios.get('http://localhost:5000/api/taskapi')
      this.setState({ tasks: res.data })
    }
    catch (e) {

    }
  }

  selectItem = (eventKey, event) => {
    this.setState({ filterBy: eventKey })
  }

  search = (e) => {
    const { value } = e.target
    this.setState({ filter: value })
  }

  async onCreate(task) {
    try {
      const r = await axios.post("http://localhost:5000/api/taskapi", task);
      const newTasks = this.state.tasks;
      newTasks.push(r.data);
      this.setState({tasks: newTasks});
    }
    catch (e) {
      console.log(e)
    }
  }

  render() {
    const { tasks, filter, filterBy } = this.state
    return (
      <div className="container">
        <div className="row">
          <div className="col-8">
            <SearchInput
              onInput={this.search}
            />
          </div>
          <div className="col-2">
            <PriorityFilter className="max-width" onSelect={this.selectItem} />
          </div>
          <div className="col-2">
            <button className="btn btn-primary max-width">Add New Task</button>
          </div>
        </div>
        <br />
        <CreateTaskForm onCreate={this.onCreate.bind(this)} />

        <br></br>
        <div className="row">
          <div className="col-12">
            <TaskList tasks={tasks} filter={filter} filterBy={filterBy} />
          </div>
        </div>
      </div>
    );
  }
}
export default Task;