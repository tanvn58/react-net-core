import React from 'react'
import { Table } from 'react-bootstrap'

export const TaskList = ({ tasks, filter, filterBy }) => {
  let filtered = tasks
  
  if (filter || filterBy) {
    filtered = tasks.filter(it => it.name != null ? it.name.includes(filter != undefined ? filter.toLowerCase() : "") : false)
    if (filterBy != ""){
      filtered = filtered.filter(x => x.priority.rank == filterBy)
    }
  }
  return (
    <Table striped bordered hover variant="dark">
      <thead>
        <tr>
          <th>Name</th>
          <th>Description</th>
          <th>Priority</th>
          <th>Created Date</th>
        </tr>
      </thead>
      <tbody>
        {filtered.map((x) => (
          <tr key={"task-" + x.id}>
            <td>{x.name}</td>
            <td>{x.description}</td>
            <td>{x.priority.name}</td>
            <td>{x.createdAt}</td>
          </tr>
      ))}
      </tbody>
    </Table>
  )
}