import React from 'react';
import './navigation.scss';

class Navigation extends React.Component {
  render() {
    return(
      <div className="navigation-main">
        <ul>
          <li>Home</li>
          <li>About Us</li>
          <li>Customers</li>
        </ul>
      </div>
    );
  }
}

export default Navigation;