import React from 'react'
import ReactDOM from 'react-dom'
import Task from './components/TaskComponent/Task'
import Navigation from './components/NavigationComponent/navigation'
import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render(<Navigation/>, document.getElementById("navigation"))
ReactDOM.render(<Task/>, document.getElementById("root"))