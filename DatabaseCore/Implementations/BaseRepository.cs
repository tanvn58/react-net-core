﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace DatabaseCore.Implementations
{
    public class BaseRepository<T> where T : class
    {
        protected readonly IConfiguration _config;
        protected readonly string _tableName;
        public BaseRepository(IConfiguration config, string tableName)
        {
            _config = config;
            _tableName = tableName;
        }

        public virtual async Task<T> Create(T record)
        {
            using (var conn = new SqlConnection(_config.GetConnectionString("Task")))
            {
                try
                {
                    var props = record.GetType().GetProperties();
                    var sql = "insert into " + _tableName + " (@$%clm) values (@$%vl); SELECT SCOPE_IDENTITY();";
                    var propNameListing = "";
                    var propValueListing = "";
                    foreach (var prop in props)
                    {
                        if (prop.Name.ToLower() == "id")
                        {
                            continue;
                        }

                        if (prop.GetValue(record) != null)
                        {
                            propNameListing += prop.Name + ", ";
                            propValueListing += "'" + prop.GetValue(record)?.ToString() + "', ";
                        }
                    }

                    propNameListing = propNameListing.TrimEnd(' ', ',');
                    propValueListing = propValueListing.TrimEnd(' ', ',');
                    sql = sql.Replace("@$%clm", propNameListing).Replace("@$%vl", propValueListing); ;
                    var resultId = await conn.QuerySingleAsync<int>(sql);
                    return Get(resultId).Result;
                }
                catch (Exception e)
                {
                    return null;
                }
            }
        }

        public virtual async Task<bool> Delete(object id)
        {
            using (var conn = new SqlConnection(_config.GetConnectionString("Task")))
            {
                var sql = "delete from " + _tableName + " where Id = @Id;";
                var result = await conn.ExecuteAsync(sql, new { Id = id });
                return result > 0;
            }
        }

        public virtual async Task<T> Get(object id)
        {
            using (var conn = new SqlConnection(_config.GetConnectionString("Task")))
            {
                var sql = "select * from " + _tableName + " where Id = @Id";
                var result = await conn.QuerySingleOrDefaultAsync<T>(sql, new { Id = id });
                return result;
            }
        }

        public virtual async Task<IEnumerable<T>> GetAll()
        {
            using (var conn = new SqlConnection(_config.GetConnectionString("Task")))
            {
                var sql = "select * from " + _tableName;
                var result = await conn.QueryAsync<T>(sql);
                return result;
            }
        }

        public virtual async Task<T> Update(T record)
        {
            using (var conn = new SqlConnection(_config.GetConnectionString("Task")))
            {
                var id = record.GetType().GetProperty("Id").GetValue(record);
                var sql = "update " + _tableName + " set ";
                var values = "";
                var whereClause = " where Id=@Id";
                var props = record.GetType().GetProperties();
                foreach (var prop in props)
                {
                    if (prop.Name.ToLower() == "id")
                    {
                        continue;
                    }
                    if (prop.GetValue(record) != null)
                    {
                        values += prop.Name + "='" + prop.GetValue(record) + "',";
                    }
                }
                sql += values.TrimEnd(',');
                sql += whereClause;
                var result = await conn.ExecuteAsync(sql, new { Id = id });
                if (result > 0)
                {
                    return Get(id).Result;
                }

                return null;
            }
        }
    }
}
