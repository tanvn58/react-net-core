﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using DatabaseCore.Interfaces;
using DatabaseCore.Structure;
using Microsoft.Extensions.Configuration;

namespace DatabaseCore.Implementations
{
    public class PriorityRepository : BaseRepository<Priority>, IPriorityRepository
    {
        public PriorityRepository(IConfiguration config) : base(config, "Priorities") {}

        //public async Task<Priority> Get(int id)
        //{
        //    using (var conn = new SqlConnection(_config.GetConnectionString("Task")))
        //    {
        //        var sql = "select * from Priorities where Id = @Id";
        //        var result = await conn.QuerySingleAsync<Priority>(sql, new { Id = id });
        //        return result;
        //    }
        //}
    }
}
