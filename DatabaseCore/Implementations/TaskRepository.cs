﻿using DatabaseCore.Interfaces;
using DatabaseCore.Models;
using DatabaseCore.Structure;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;

namespace DatabaseCore.Implementations
{
    public class TaskRepository : BaseRepository<Task>, ITaskRepository
    {
        private readonly IPriorityRepository _priorityRepository;
        public TaskRepository(IConfiguration config, IPriorityRepository priorityRepository) : base(config, "Tasks")
        {
            _priorityRepository = priorityRepository;
        }

        public TaskModel GetModel(object id)
        {
            var task = Get(id).Result;
            var priority = _priorityRepository.Get(task.PriorityId).Result;
            var model = new TaskModel(task);
            model.Priority.Id = priority.Id;
            model.Priority.Name = priority.Name;
            model.Priority.Rank = priority.Rank;

            return model;
        }

        public IEnumerable<TaskModel> GetModels()
        {
            var allTasks = GetAll().Result;
            return allTasks.Select(x => {
                var priority = _priorityRepository.Get(x.PriorityId).Result;
                var model = new TaskModel(x);
                model.Priority.Id = priority.Id;
                model.Priority.Name = priority.Name;
                model.Priority.Rank = priority.Rank;
                return model;
            });
        }
    }
}
