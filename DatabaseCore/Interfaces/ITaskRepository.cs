﻿using DatabaseCore.Models;
using DatabaseCore.Structure;
using System.Collections.Generic;

namespace DatabaseCore.Interfaces
{
    public interface ITaskRepository : IBaseRepository<Task>
    {
        TaskModel GetModel(object id);
        IEnumerable<TaskModel> GetModels();
    }
}
