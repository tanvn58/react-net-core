﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace DatabaseCore.Interfaces
{
    public interface IBaseRepository<T> where T : class
    {
        Task<T> Get(object id);
        Task<IEnumerable<T>> GetAll();
        Task<T> Create(T record);
        Task<T> Update(T record);
        Task<bool> Delete(object id);
    }
}
