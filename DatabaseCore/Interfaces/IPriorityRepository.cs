﻿using DatabaseCore.Structure;

namespace DatabaseCore.Interfaces
{
    public interface IPriorityRepository : IBaseRepository<Priority>
    {
    }
}
