﻿using DatabaseCore.Implementations;
using DatabaseCore.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace DatabaseCore
{
    public static class DependencyConfiguration
    {
        public static void RegisterTypes(this IServiceCollection collection)
        {
            collection.AddTransient<IPriorityRepository, PriorityRepository>();
            collection.AddTransient<ITaskRepository, TaskRepository>();
        }
    }
}
