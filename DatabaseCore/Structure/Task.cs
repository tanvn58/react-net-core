﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseCore.Structure
{
    public class Task
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int PriorityId { get; set; }
        public string Description { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int CreatedBy { get; set; }
    }
}
