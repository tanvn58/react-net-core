﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseCore.Structure
{
    public class Priority
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Rank { get; set; }
    }
}
