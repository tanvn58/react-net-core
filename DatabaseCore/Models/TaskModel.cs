﻿using DatabaseCore.Structure;
using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseCore.Models
{
    public class TaskModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Priority Priority { get; set; }
        public string Description { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int CreatedBy { get; set; }

        public TaskModel(Task record)
        {
            Id = record.Id;
            Name = record.Name;
            Description = record.Description;
            CreatedAt = record.CreatedAt;
            CreatedBy = record.CreatedBy;
            Priority = new Priority();
    }
    }
}
