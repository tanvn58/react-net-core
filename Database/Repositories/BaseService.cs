﻿using Database.Interfaces;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Database.Repositories
{
    public abstract class BaseService<T> : IBaseService<T> where T : class
    {
        protected TasksContext _context;
        protected ILog _log;
        public BaseService()
        {
            _context = new TasksContext();
        }

        public virtual T Create(T record)
        {
            try
            {
                _context.Set<T>().Add(record);
                return record;
            } 
            catch(Exception e)
            {
                _log.Error(e.Message);
                return null;
            }
        }

        public virtual bool Delete(object key)
        {
            try
            {
                var record = _context.Set<T>().Find(key);
                _context.Set<T>().Remove(record);

                return true;
            }
            catch (Exception e)
            {
                _log.Error(e.Message);
                return false;
            }
        }

        public virtual T Get(object key)
        {
            return _context.Set<T>().Find(key);
        }

        public virtual IEnumerable<T> GetAll()
        {
            return _context.Set<T>();
        }
    }
}